# Automatically shutdown at specified time

Example crontabs to create (to be automated):

```
user:
-----
55 21 * * * export DISPLAY=:0 && /usr/bin/notify-send -u critical "5 MINUTES REMAINING"
45 21 * * * export DISPLAY=:0 && /usr/bin/notify-send -u critical "Shutting Down in 15 minutes"
30 21 * * * export DISPLAY=:0 && /usr/bin/notify-send "Shutting down in 30 minutes"
```

```
root:
-----
0 22 * * * /sbin/shutdown -h now
@reboot /home/pi2/workspace/autoshutdown/wrapper.sh
```
