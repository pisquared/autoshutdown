#!/bin/bash
function schedule_shutdown() {
    echo "shuting down in 5 minutes"
    /bin/sleep 5m
    /sbin/shutdown -h now
}

H=$(date +%H)
if (( 0 <= 10#$H && 10#$H < 6 )); then
    echo between 12AM and 6AM
    schedule_shutdown
elif (( 22  <= 10#$H && 10#$H <= 23 )); then
    echo between 10PM and 12PM
    schedule_shutdown
else
    echo its okay
fi

