import datetime
import os

OFF_TIMES = {
    1: [("00:00", "06:00"), ("22:30", "24:00")],
    2: [("00:00", "06:00"), ("22:30", "24:00")],
    3: [("00:00", "06:00"), ("22:30", "24:00")],
    4: [("00:00", "06:00"), ("22:30", "24:00")],
    5: [("00:00", "06:00"), ],
    6: [],
    7: [("22:30", "24:00")],
}


def time_to_int(timestr):
    hours, minutes = timestr.split(':')
    return 60 * int(hours) + int(minutes)


def is_time_between(now, start, end):
    return time_to_int(start) <= time_to_int(now) < time_to_int(end)


def is_now_offtime():
    now = datetime.datetime.now()
    weekday = now.isoweekday()
    now_hm = now.strftime("%H:%M")
    weekday_off_times = OFF_TIMES.get(weekday)
    for off_time in weekday_off_times:
        if is_time_between(now_hm, off_time[0], off_time[1]):
            return True
    return False


def main():
    if is_now_offtime():
        os.popen("sudo pm-suspend")


if __name__ == "__main__":
    main()
